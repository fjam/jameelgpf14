package edu.Jameel.lab3.intro;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class IntroToLibGDX extends ApplicationAdapter{

	private SpriteBatch spriteBatch;
	private Sprite bug;
	private Sprite chest;
	private float rotDeg;	
	private boolean turn = false;
	Vector2 v;
	
	@Override
	public void create() {
	    // Game Initialization  
	    v = new Vector2(Gdx.graphics.getWidth() - 0, Gdx.graphics.getHeight() - 0); 
	    v.nor();
	    v.scl(10);

	    spriteBatch = new SpriteBatch(); 
	    bug = new Sprite(new Texture("EnemyBug.png"));
	    bug.setSize(50, 85);
	    bug.setOrigin(bug.getWidth() / 2.0F , bug.getHeight() / 2.0F);
	    bug.setOrigin(0,0);
	    bug.setPosition(1,1);
	    bug.rotate(v.angle());
	    
	    chest = new Sprite(new Texture("ChestClosed.png"));
	    chest.setSize(50.0F, 85.0F);
	    chest.setOrigin(this.chest.getWidth() / 2.0F, this.chest.getHeight() / 2.0F);
	    chest.setPosition(100.0F, 100.0F);
	    
	    rotDeg = 180.0F;
	}

	@Override
	public void render() {
	    // Game Loop

	    Gdx.gl.glClearColor(0.7f, 0.7f, 0.2f, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    spriteBatch.begin();        

	    if(bug.getX() >= (int)(Gdx.graphics.getWidth()) && bug.getY() >= (int)(Gdx.graphics.getHeight())){
	    	bug.rotate(rotDeg);

	        turn = !turn;
	    }	    
	    else if(bug.getX() <= 0 && bug.getY() <= 0){
	    	bug.rotate(rotDeg);
	        	    	
	        turn = !turn;
	    }

	    if(!turn){          
	        bug.translate(v.x * Gdx.graphics.getDeltaTime(), v.y * Gdx.graphics.getDeltaTime());
	    }
	    else{
	        bug.translate(-(v.x * Gdx.graphics.getDeltaTime()), -(v.y * Gdx.graphics.getDeltaTime()));
	    }
	    
	    chest.draw(this.spriteBatch);
	    bug.draw(spriteBatch);

	    spriteBatch.end();
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
		super.dispose();
	}
}