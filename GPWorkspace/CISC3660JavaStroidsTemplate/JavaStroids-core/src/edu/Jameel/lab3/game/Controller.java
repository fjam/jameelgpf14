package edu.Jameel.lab3.game;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.Vector2;

import edu.Jameel.lab3.gameobjects.Asteroid;
import edu.Jameel.lab3.gameobjects.GameObject;
import edu.Jameel.lab3.gameobjects.Missile;
import edu.Jameel.lab3.gameobjects.Ship;

public class Controller {
	
	ArrayList<GameObject> drawableObjects; 
	Ship ship;
	private float screenHeight;
	private Sound thrustersSound;
	private Music backgroundNoise;
	private boolean shipCrashed;
	private Sound explosionSound;
	 private float explosionX;
	  private float explosionY;
	  public boolean asteroidExplosion;

	
	public Controller(){
		drawableObjects = new ArrayList<GameObject>(); 
		initShip();
		initAsteroids(10);
		screenHeight = Gdx.graphics.getHeight();
		initSound();
	}
	
	public boolean isShipCrashed(){
		return shipCrashed;
	}
	  public float getExplosionX()
	  {
	    return this.explosionX;
	  }
	  
	  public float getExplosionY()
	  {
	    return this.explosionY;
	  }
	
	//This method initializes the ship.
	private void initShip(){		
		int w = Constants.SHIP_WIDTH; 
		int h = Constants.SHIP_HEIGHT; 
		Pixmap pmap = new Pixmap(w, h, Format.RGBA8888);
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(0, h, w/2, 0);
		pmap.drawLine(w, h, w/2, 0);
		pmap.drawLine(1, h-1, w, h-1);
		ship = new Ship(new Texture(pmap), 100, 100);
		drawableObjects.add(ship);
	}
	
	private void initSound()
	  {
	    thrustersSound = Gdx.audio.newSound(Gdx.files.internal("125810__robinhood76__02578-rocket-start.wav"));
	    backgroundNoise = Gdx.audio.newMusic(Gdx.files.internal("132150__soundsodd__interior-spaceship.mp3"));
	    backgroundNoise.setLooping(true);
	    backgroundNoise.play();
	    backgroundNoise.setVolume(0.5F);
	   // fireSound = Gdx.audio.newSound(Gdx.files.internal("250154__robinhood76__05433-stinger-rocket-deploy.wav"));
	    
	   //explosionSound = Gdx.audio.newSound(Gdx.files.internal("110113__ryansnook__medium-explosion.wav"));
	  }
	
	
	
	private void initAsteroids(int num){
		Random rand = new Random();
		for(int i = 0; i<num; i++){
			Asteroid asteroid = new Asteroid(new Texture("Asteroid_tex.png"));
			asteroid.sprite.setPosition(rand.nextInt(Gdx.graphics.getWidth()), rand.nextInt(Gdx.graphics.getHeight()));
			asteroid.sprite.setOrigin(asteroid.sprite.getWidth() / 2, asteroid.sprite.getHeight() / 2);
			asteroid.setRotVel(rand.nextFloat()*8-4);
			drawableObjects.add(asteroid);
		}
	}
	
	public void update(){
		processKeyboardInput();
		processMouseInput();
		
			// Update Asteroids
		for(GameObject gObg : drawableObjects){
			
			if(gObg instanceof Asteroid){
				((Asteroid) gObg).update(Gdx.graphics.getDeltaTime()); 
				
				if ((ship.sprite.getBoundingRectangle().overlaps(((Asteroid)gObg).sprite.getBoundingRectangle())) && (!shipCrashed))
		        {
		          shipCrashed = true;
		          explosionSound.play();
		          thrustersSound.stop();
		          explosionX = ship.sprite.getX();
		          explosionY = ship.sprite.getY();
		        }
			}
			
			// Update Missiles
			if(gObg instanceof Missile){
				((Missile) gObg).update(Gdx.graphics.getDeltaTime());
			}			
		}
		// Update ship
		ship.update(Gdx.graphics.getDeltaTime());
		
		
	}
	
	private void processMouseInput(){
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			ship.face(new Vector2(Gdx.input.getX() - ship.sprite.getX(), -(screenHeight - Gdx.input.getY() - ship.sprite.getY())));
		}
	}
	
	private void processKeyboardInput(){
		if (Gdx.app.getType() != ApplicationType.Desktop) return; // Just in case :)
		
		//When the UP key is pressed the ship moves forward
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			ship.moveForward(Gdx.graphics.getDeltaTime());
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.UP)){
			thrustersSound.play(0.5f);
		}
		
		//When SPACE is pressed a missile shoots
		if(Gdx.input.isKeyPressed(Keys.SPACE)){
			initMissile();
		}
	}
	
	private void initMissile(){
		int w = Constants.SHIP_WIDTH;
		int h = Constants.SHIP_HEIGHT;
		
		Pixmap pmap = new Pixmap(w,h, Format.RGB565);
		pmap.setColor(1,1,1,1);
		pmap.drawLine(w/2, 0, w/2, h);
		drawableObjects.add(new Missile(new Texture(pmap), ship.getDirection(), ship.getPosition()));
	}
	
	public ArrayList<GameObject> getDrawableObjects(){
		return drawableObjects;
	}
	
	public void dispose(){
		if(thrustersSound != null){
			thrustersSound.dispose();
		}
		if(backgroundNoise != null){
			backgroundNoise.dispose();
		}
	}
	
}
