package edu.Jameel.lab3.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import edu.Jameel.lab3.gameobjects.GameObject;

public class Renderer {
	
	private SpriteBatch spriteBatch;
	private Controller control;
	BitmapFont font;
	Texture bg1, bg2;
	float bg1XPos, bg2XPos;
	Animation explosionAnim;
	  Texture explosionSheet;
	  TextureRegion[] explosionFrames;
	  TextureRegion currentFrameExplosion;
	  float shipExplosionStateTime;
	
	public Renderer(Controller c){
		control = c;
		spriteBatch = new SpriteBatch(); 
		font = new BitmapFont();
		
	    bg1 = new Texture("nebula-space-low.jpg");
	    bg2 = new Texture("nebula-space-low.jpg");
	    bg1XPos = 0.0F;
	    bg2XPos = this.bg1.getWidth();
	    
	    
	    
	    this.explosionSheet = new Texture(Gdx.files
	    	      .internal("explosionSheet.png"));
	    	    TextureRegion[][] tmp = TextureRegion.split(
	    	      this.explosionSheet, this.explosionSheet.getWidth() / 5, 
	    	      this.explosionSheet.getHeight() / 5);
	    	    this.explosionFrames = new TextureRegion[25];
	    	    int index = 0;
	    	    for (int i = 0; i < 5; i++) {
	    	      for (int j = 0; j < 5; j++) {
	    	        this.explosionFrames[(index++)] = tmp[i][j];
	    	      }
	    	    }
	    	    this.explosionAnim = new Animation(0.05F, this.explosionFrames);
	    	    this.shipExplosionStateTime = 0.0F;
	    	    

	}
	
	public void render(){
		spriteBatch.begin();
		renderBackground();
		for(GameObject gObj : control.getDrawableObjects()){
			gObj.sprite.draw(spriteBatch);
		}
		
		
		
		
		 if ((this.control.isShipCrashed()) && 
			        (!this.explosionAnim.isAnimationFinished(this.shipExplosionStateTime)))
			      {
			        this.shipExplosionStateTime += Gdx.graphics.getDeltaTime();
			        this.currentFrameExplosion = this.explosionAnim
			          .getKeyFrame(this.shipExplosionStateTime, false);
			        this.spriteBatch.draw(this.currentFrameExplosion, 
			          this.control.getExplosionX()- Constants.SHIP_WIDTH, 
			          this.control.getExplosionY() - Constants.SHIP_HEIGHT);
			      }
		
		
		
		spriteBatch.end();
		
		
		
		
		
	}
	
	public void renderBackground()
	  {
	    spriteBatch.draw(bg1, bg1XPos, 0.0F);
	    spriteBatch.draw(bg2, bg2XPos, 0.0F);
	    if (bg1XPos + bg1.getWidth() < Gdx.graphics.getWidth()) {
	      bg2XPos = (bg1XPos + bg1.getWidth() + 1.0F);
	    }
	    if (bg2XPos + bg2.getWidth() < Gdx.graphics.getWidth()) {
	      bg1XPos = (bg2XPos + bg2.getWidth() + 1.0F);
	    }
	    bg1XPos = ((float)(bg1XPos - 0.3D));
	    bg2XPos = ((float)(bg2XPos - 0.3D));
	  }
	  
	  public void dispose()
	  {
	    this.bg1.dispose();
	    this.bg2.dispose();
	    this.spriteBatch.dispose();
	  }

}
