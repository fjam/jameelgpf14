package edu.Jameel.lab3.gameobjects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import edu.Jameel.lab3.game.Constants;

public class Asteroid extends GameObject implements Updatable{
	int screenH;
	int screenW;
	private Vector2 dirAndVel;

	private float rotationalVel;
	
	public Asteroid(Texture tex){
		screenH = Gdx.graphics.getHeight();
		screenW = Gdx.graphics.getWidth();
		
	    Random r = new Random();
		dirAndVel = new Vector2(r.nextInt(30) - 10, r.nextInt(30) - 10);
		
		sprite = new Sprite(tex);
		sprite.setSize(Constants.ASTEROIDS_SIZE, Constants.ASTEROIDS_SIZE); 
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		
		setIsDrawable(true);
	}
	
	@Override
	public void update(float deltaTime) {
		//To rotate the asteroids.
		sprite.rotate(getRotVel()); 
		
		//To move the asteroids
		sprite.translate(dirAndVel.x * deltaTime, dirAndVel.y * deltaTime);
		
		//To keep the asteroids inside the screen.
		if ((sprite.getX() <= 0.0F) || (sprite.getX() >= screenW)) {
	        dirAndVel.x = (-dirAndVel.x);
	    }
	    
		if ((sprite.getY() <= 0.0F) || (sprite.getY() >= screenH)) {
	        dirAndVel.y = (-dirAndVel.y);
		}			
	}
		
	public void setRotVel(float vel){
		rotationalVel = vel;
	}
	public float getRotVel(){
		return rotationalVel;
	}

}
