package edu.Jameel.lab3.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Ship extends GameObject implements Updatable{
	  private Vector2 direction;
	  private Vector2 targetDirection;
	  private Vector2 velocity;
	  private final float MIN_VELOCITY = 20.0F;
	  
	public Ship(Texture texture, int x, int y) {
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setPosition(x, y);
		direction = new Vector2(0, -1);
		targetDirection = new Vector2(0, -1);
		velocity = new Vector2(0, MIN_VELOCITY);
		setIsDrawable(true);
	}
	
	public void face(Vector2 targetPos){
		targetDirection = targetPos;
    }
	
	@Override
	public void update(float deltaTime) {
		double cosTheta = direction.dot(targetDirection) / targetDirection.len();
	    
		if(cosTheta > 1.0D) {
	      cosTheta = 1.0D;
	    }	    
		
		double deg = Math.acos(cosTheta);	    
	    deg = Math.toDegrees(deg) * deltaTime;
	    
	    if (direction.crs(targetDirection) > 0.0F) {
	      deg = -deg;
	    }
	    
	    sprite.rotate((float)deg);
	    direction.rotate(-(float)deg);
	    
	    sprite.translate(velocity.x * deltaTime, velocity.y * deltaTime);
	    	
    	if(velocity.len() > 20.0F){
    		velocity = velocity.scl(1.0F - deltaTime);
    	}
     }

	public void moveForward(float deltaTime) {
		//This When we are moving the spaceship forward we use delta time 		
		velocity.x += direction.x * velocity.len() * deltaTime * 2.0F;
	    velocity.y -= direction.y * velocity.len() * deltaTime * 2.0F;
	    
	    //We do not want the speed to go below 10 or over 80.
	    velocity.clamp(10.0F, 80.0F);
	}
	
	public Vector2 getDirection(){
		return direction;		
	}
	
	public Vector2 getPosition(){
		return new Vector2(sprite.getX(), sprite.getY());
	}
}
	