package edu.Jameel.lab3.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Missile extends GameObject implements Updatable{
	private Vector2 dirAndVel;
	public boolean remove;
	
	int screenWidth;
	int screenHeight;
	  
	public Missile(Texture texture, Vector2 dir, Vector2 pos){
	  sprite = new Sprite(texture);
	  sprite.setOrigin(texture.getWidth() / 2, texture.getHeight() / 2);
	  sprite.setPosition(pos.x, pos.y);
	  
	  dirAndVel = new Vector2(dir.x, -dir.y);
	  dirAndVel.scl(100.0F);
	    
	  sprite.rotate(dirAndVel.angle() - 90.0F);
	  screenWidth = Gdx.graphics.getWidth();
	  screenHeight = Gdx.graphics.getHeight();
	  setIsDrawable(true);
	  }

	public void update(float deltaT){
	  sprite.translate(dirAndVel.x * deltaT, dirAndVel.y * deltaT);
	    
	  //If the missile is out of the screen then we will remove it, so we don't have objects that are not being seen but are still using memory.
	  if ((sprite.getX() > screenWidth) || (sprite.getX() < 0.0F) || (sprite.getY() < 0.0F) || (sprite.getY() > screenHeight)) {
	    remove = true;
	  }
	 }	
}
