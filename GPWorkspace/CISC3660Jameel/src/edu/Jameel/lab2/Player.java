package edu.Jameel.lab2;

import java.util.*;

import edu.Jameel.lab1.Card;
import edu.Jameel.lab1.Rank;

public class Player {
	private ArrayList<Card> hand;
	
	public Player(){
		hand = new ArrayList<Card>();
	}
	
	public void receiveCard(Card c){
		hand.add(c);
	}
	
	public ArrayList<Card> getHand(){
		return hand;
	}
	
	//gets what is in the hand
	public String getHandString(){
		String  handStr = "";
		for(Card c : hand){
			handStr += c.getRank() + " ";
		}
		return handStr;
	}
	
	//Adds cards to the hand and does the ace conversions. 
	public int addCards(){
		int sum = 0;
		
		
		for (Card c: this.hand){
			//This statement is to check if the values of the jack, queen, and king.
			if (c.getRank()== Rank.JACK || c.getRank()== Rank.QUEEN ||  c.getRank()== Rank.KING){
				sum += 10;
			}
			//This part check if there is an ace, jack, queen and king. If so then, it add the ordinals +1 to the sum. 
			else if (c.getRank() != Rank.ACE && c.getRank() != Rank.JACK && c.getRank() != Rank.QUEEN && c.getRank() != Rank.KING ){
				sum += c.getRank().ordinal() + 1;
			}
				
		}
		
		//This if to check if the ace should count as a 1 or 11
		for (Card c: this.hand){
			//if sum is <= to 10 then ace counts as 11
			if (c.getRank() == Rank.ACE && sum <= 10)
				sum += 11;
			//if sum is > 10 then ace counts as 1
			else if (c.getRank() == Rank.ACE && sum > 10)
				sum += 1;
		}
		
		return sum;
	}
	
}
