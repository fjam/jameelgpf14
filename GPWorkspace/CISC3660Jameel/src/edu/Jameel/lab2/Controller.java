package edu.Jameel.lab2;

public class Controller {
	MessagePanel msg;
	boolean playing;
	State state;
	
	public Controller(){}
	
	public void init(){
		state = new State();
		msg.outputMessage(1, "It is your turn. Your hand: " 
				+ state.getPlayer(1).getHandString()
				+ " Dealer's hand: "
				+ state.getDealer().getHandString());
		msg.outputMessage(2, Constants.WAIT_MSG);
		msg.outputMessage(3, Constants.WAIT_MSG);		
	}
	
	public void clientRequest(char c){
		if(playing){
			changeState(c);
			return;
		}
		init();
		playing = true;
	}
	
	public void setMessenger(MessagePanel message){
		msg = message;
		msg.showMenu();
	}
	
	private void showErrorMsgPlayerKey(char c){
		int player = -1;
		if(c == 'q' || c == 'w'){
			player = 1;
		}else if(c == 'a' || c == 's'){
			player = 2;
		}else if(c == 'z' || c == 'x'){
			player = 3;
		}
		msg.outputMessage(player, Constants.WRONG_TURN_MSG);	
	}	
	
	private void changeState(char c){
		
		//if a button  is pressed it will check if it is player 1's turn, if so then it will display the appropriate message.
		if (c == 'q'){
			if (state.getTurn() == 1) {
				state.getPlayer(1).receiveCard(state.getDeck().dealCard());
				msg.outputMessage(1, "Your turn. You have: " + state.getPlayer(1).getHandString() + " Dealer has: " + state.getDealer().getHandString());				
			}else {
				showErrorMsgPlayerKey ('a');
				showErrorMsgPlayerKey ('z');
				showErrorMsgPlayerKey ('s');
				showErrorMsgPlayerKey ('x');
			}			
		}else if (c == 'z'){
			if (state.getTurn() == 3){
				state.getPlayer(3).receiveCard(state.getDeck().dealCard());
				msg.outputMessage(3,"Your turn. You have: " + state.getPlayer(3).getHandString() + " Dealer has: " + state.getDealer().getHandString());
			}else {
				showErrorMsgPlayerKey ('a');
				showErrorMsgPlayerKey ('z');
				showErrorMsgPlayerKey ('s');
				showErrorMsgPlayerKey ('q');
			}			
		}else if (c == 'a'){
			if (state.getTurn() == 2){
				state.getPlayer(2).receiveCard(state.getDeck().dealCard());
				msg.outputMessage(2,"Your turn. You have: " + state.getPlayer(2).getHandString() + " Dealer has: " + state.getDealer().getHandString());
			}else {
				showErrorMsgPlayerKey ('a');
				showErrorMsgPlayerKey ('z');
				showErrorMsgPlayerKey ('q');
				showErrorMsgPlayerKey ('x');
			}			
		}else if (c == 's'){
			if (state.getTurn() == 2){
				state.nextTurn();
				msg.outputMessage(3,"Your turn. You have: " + state.getPlayer(3).getHandString() + " Dealer has: " + state.getDealer().getHandString());
				showErrorMsgPlayerKey ('w');
				showErrorMsgPlayerKey ('a');
			}else {
				showErrorMsgPlayerKey ('a');
				showErrorMsgPlayerKey ('z');
				showErrorMsgPlayerKey ('q');
				showErrorMsgPlayerKey ('x');
			}			
		}else if (c == 'w'){
			if (state.getTurn() == 1){
				state.nextTurn();
				msg.outputMessage(2,"Your turn. You have: " + state.getPlayer(2).getHandString() + " Dealer has: " + state.getDealer().getHandString());
			}else {
				showErrorMsgPlayerKey ('a');
				showErrorMsgPlayerKey ('z');
				showErrorMsgPlayerKey ('s');
				showErrorMsgPlayerKey ('x');
			}			
		}else if (c == 'x'){
			if (state.getTurn() == 3){
				if (state.getPlayer(1).addCards() <= 21 && state.getPlayer(2).addCards() <= 21 && state.getPlayer(3).addCards() <= 21){
					while (state.getPlayer(0).addCards() <= state.getPlayer(1).addCards() && state.getPlayer(0).addCards() <= state.getPlayer(2).addCards() && state.getPlayer(0).addCards() <= state.getPlayer(3).addCards()){
						state.getPlayer(0).receiveCard(state.getDeck().dealCard());
					}
				}
					
				System.out.println(state.getPlayer(0).getHandString());
				System.out.println(state.getPlayer(1).getHandString());
				System.out.println(state.getPlayer(2).getHandString());
				System.out.println(state.getPlayer(3).getHandString());
				 
				endResult(0,1);
				endResult(0,2);
				endResult(0,3);
				
			    playing = false;
			}		
		}else
			return;
	}
	
	//This is the end result of the game. Who won and who lost.
	public void endResult(int x, int y){
		if (state.getPlayer(y).addCards() == 21 && state.getPlayer(y).getHandString().length() <=10){
			if(state.getPlayer(x).addCards() == 21){	
				if (state.getPlayer(x).getHandString().length() > 10)
					msg.outputMessage(y,Constants.BLACKJACK);
			}
			else 
				msg.outputMessage(y,Constants.BLACKJACK);
		} 			
		
		else if ((state.getPlayer(x).addCards() > state.getPlayer(y).addCards() || state.getPlayer(y).addCards() > 21) && state.getPlayer(x).addCards() <= 21)
			msg.outputMessage(y,Constants.LOST);
		else if ((state.getPlayer(x).addCards() < state.getPlayer(y).addCards() || state.getPlayer(x).addCards() > 21) && state.getPlayer(y).addCards() <= 21)
			msg.outputMessage(y, Constants.WON);
		else if (state.getPlayer(x).addCards() == state.getPlayer(y).addCards() || (state.getPlayer(x).addCards() > 21)	&& state.getPlayer(y).addCards() > 21)
			msg.outputMessage(y, Constants.LOST);
	}
	
}
