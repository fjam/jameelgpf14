package edu.Jameel.lab2;

import edu.Jameel.lab1.Deck;

public class State {
	private byte turn;
	private Player[] players;
	private Deck deck;
	
	public State(){
		init();
	}
	
	public void init(){
		turn = 1;
		deck = new Deck();
		deck.initFullDeck();
		deck.shuffle();
		players = new Player[3];
		players[0] = new Player();
		players[0].receiveCard(deck.dealCard());
		for(int i = 1 ; i < players.length; i++){
			players[i] = new Player();
			players[i].receiveCard(deck.dealCard());
			players[i].receiveCard(deck.dealCard());
		}
	}
	
	public Player getPlayer(int playerNum){
		return players[playerNum];
	}
	
	public Player getDealer(){
		return players[0];
	}
	
	public Deck getDeck(){return deck;}
	public byte getTurn(){return turn;}
	public void nextTurn(){turn++;}
}
