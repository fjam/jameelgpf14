package edu.Jameel.lab1;

public class Card {
	private Suit suit;
	private Rank rank;
	
	public Card(Suit suit, Rank rank){
		this.suit = suit;
		this.rank = rank;
	}
	
	public Suit getSuit(){
		return suit;
	}
	
	public void setSuit(Suit suit){
		this.suit = suit;
	}
	
	public Rank getRank(){
		return rank;
	}
	public void setRank(Rank rank){
		this.rank = rank;
	}
	
	//Compares the rank and suit of two cards and returns true or false.
	public boolean compare(Card c1, Card c2){
		if (c1.getRank() == c2.getRank() && c1.getSuit() == c2.getSuit()){
			return true;
		}
		return false;
	}
	
	//Compares the rank of two cards and returns true or false
	public boolean compareRank(Card c1, Card c2){
		if (c1.getRank() == c2.getRank()){
			return true;
		}
		return false;
	}
	
	//Compares the suit of two cards and returns true or false
	public boolean compareSuit(Card c1, Card c2){
		if (c1.getSuit() == c2.getSuit()){
			return true;
		}
		return false;
	}
}
