package edu.Jameel.lab1;

import java.util.*;

public class Main {
	public static void main(String[] args){
		lab1();
	}
	
	public static void lab1(){
		Deck deck = new Deck();
		
		ArrayList<Card> deck1 = new ArrayList<Card>();
		ArrayList<Card> deck2 = new ArrayList<Card>();
		
		//Fill up the two arrays with random cards from the deck.
		while(deck.getNumberOfCardsRemaining() != 0){
			deck1.add(deck.dealCard());  
			deck2.add(deck.dealCard());
		}
	
		//Declare array to hold the sum of the ordinals 
		int[] sum = new int[2]; 
				
		//goes through each card and adds the rank ordinals for deck 1
		for(Card c : deck1){
			sum[0] += c.getRank().ordinal();			
		}
		//goes through each card and adds the rank ordinals for deck 2
		for(Card c : deck2){
			sum[1] += c.getRank().ordinal();
		}
		
		
		//Printing output
		System.out.println("Deck One: " + sum[0]);
		System.out.println("Deck Two: " + sum[1]);
		
		System.out.println("------------------------");
		
		System.out.println(sum[0] > sum[1] ? "Deck One is the Winner" : 
			sum[0] == sum[1] ? "They are equal " : "Deck Two is the Winner");		
	} 
	
	public static void testLab1(){
		Deck deck = new Deck();
		System.out.println(deck.toString());
	}

}