package edu.Jameel.lab1;

import java.util.*;

public class Deck {
	private ArrayList<Card> deck = new ArrayList<Card>();
	
	// fills the deck with cards.
	public Deck(){
		for(Rank r: Rank.values()){
			for(Suit s : Suit.values()){
	             deck.add(new Card(s, r));
			}    
		}        
	}
	
	public void initFullDeck(){
		deck.removeAll(deck);
		for(Rank r : Rank.values()){
			for(Suit s : Suit.values()){
				deck.add(new Card(s, r));
			}
		}
	}
	
	//removes all cards from deck
	public void initEmptyDeck(){
		deck.removeAll(deck);
	}
	
	//returns the deck.
	public List<Card> getDeck(){
		return Collections.unmodifiableList(deck);
	}
	
	//removes a card from the deck.
	public Card removeCardDeck(int index){
		return deck.remove(index);
	}
	
	//adds a card to the deck
	public void addOneCard (Card c){
		deck.add(c);
	}
	
	//removes the a card that you are looking for
	public void removeAParticularCard(Card c){
		for(int i = deck.size() -1; i >= 0; i--){
			if(deck.get(i).getRank() == c.getRank() && deck.get(i).getSuit() == c.getSuit()){
				deck.remove(i);
				break;
			}
		}		
	}
	
	//Empties the deck
	public void removeAllCardsOfRank(Rank r){
		for(int i = deck.size() -1; i >= 0; i--){
			if(deck.get(i).getRank() == r){
				deck.remove(i);
			}
		}
	}
	
	//returns cards from the deck in order
	public ArrayList<Card> getOrderedCards(){
		ArrayList<Card> orderedList = new ArrayList<Card>();
		for (Rank r : Rank.values()){
			for (Suit s : Suit.values()){
				Card c = new Card (s,r);
				for (int i = 0; i < getNumberOfCardsRemaining(); i++) {
					if (c.compareRank(c, getDeck().get(i)))
						orderedList.add(c);
				}
			}
		}
		return orderedList;
	}
	
	//Returns the remaining number of cards
	public int getNumberOfCardsRemaining(){
		return deck.size();
	}
	
	//Returns a random card from the deck
	public Card dealCard(){
		shuffle();
		return deck.remove(0);
	}
	
	//Shuffles the deck
	public void shuffle(){
		long seed = System.nanoTime();	
		Collections.shuffle(deck, new Random(seed));
	}
	
	@Override
	//Prints all the cards in the deck.
	public String toString(){
		String s = "";
		for(Card c : deck){
			s = s + c.getSuit().toString() + " " + c.getRank().toString() + "\n";
		}
		return s;
	}
}
